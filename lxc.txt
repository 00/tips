インストール
sudo apt update && sudo apt install -y lxc

停止から起動
sudo lxc-stop --name mycontainer && \
sudo lxc-destroy --name mycontainer && \
sudo lxc-create -n mycontainer -t download -- -d mint -r virginia -a amd64 && \
sudo lxc-start --name mycontainer && \
sudo lxc-attach --name mycontainer

コンテナの作成
ダウンロードテンプレート: https://images.linuxcontainers.org/
sudo lxc-create -n mycontainer -t download -- -d mint -r victoria -a amd64
(sudo lxc-create --name mycontainer --template download -- --dist alpine --release 3.19 --arch amd64)

コンテナの起動
sudo lxc-start --name mycontainer

コンテナの状態一覧
sudo lxc-ls --fancy

コンテナ内へログイン
sudo lxc-attach --name mycontainer

コンテナの停止
sudo lxc-stop --name mycontainer

コンテナの削除
sudo lxc-destroy --name mycontainer

ディスクの場所
/var/lib/lxc/mycontainer/rootfs

// basic
rg --pre xls2txt -g *.{xls,xlsx} -l [pattern] [path]

// disable regex
rg --pre xls2txt -g *.{xls,xlsx} -l -F [pattern] [path]

// print all target files (for debug)
rg --pre xls2txt -g *.{xls,xlsx} -files [path]

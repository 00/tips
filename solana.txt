// encode valid punctuation: *-_

import { createSignal, Show } from "solid-js";
import { Title } from "@solidjs/meta";
import { PublicKey, Keypair } from "@solana/web3.js";
import { encodeURL, createQR, findReference, validateTransfer } from "@solana/pay";
import BigNumber from "bignumber.js";

export default function Home() {
  const [isClicked, setIsClicked] = createSignal(false);
  const [url, setUrl] = createSignal("");

  const handleClick = async () => {
    const recipient = new PublicKey("aa");
    const amount = new BigNumber(0.0001);
    const seed = new TextEncoder().encode("aa");  // debug
    const reference = Keypair.fromSeed(seed).publicKey;  // debug
    // const reference = new Keypair().publicKey;
    const label = "aa";
    const message = "aa";
    // memo max len: 318 (rsa 3072 bit)
    const memo = "aa";
    setUrl(encodeURL({ recipient, amount, reference, label, message, memo }));
    console.log(encodeURL({ recipient, amount, reference, label, message, memo }));
    console.log("reference:", reference.toJSON()); // debug
    console.log("reference:", reference.toString()); // debug
    const qrCode = createQR(url(), 512, "#ddd", "#000");
    qrCode.update({
      dotsOptions: {
        type: "square"
      },
      cornersSquareOptions: {
        type: "square"
      },
      cornersDotOptionsHelper: {
        type: "square"
      }
    });
    const element = document.getElementById("qr-code");
    qrCode.append(element);
    setIsClicked(true);
  }

  return (
    <main>
      <Title>SOL</Title>
      <h1>SOL</h1>
      <Show when={isClicked()} fallback={<button onClick={() => handleClick()}>Click me</button>}>
        <p>以下のQRコードをPhantomで読み取り、お支払いをお願いいたします。</p>
      </Show>
      <div id="qr-code"></div>
      <Show when={isClicked()}>
        <p>スマホの方は以下のURLからアクセスしてください。</p>
        <a href={url().href}>{url().href}</a>
      </Show>
    </main>
  );
}

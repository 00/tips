https://nginx.org/en/linux_packages.html#Ubuntu

sudo apt update && \
sudo apt install -y curl gnupg2 ca-certificates lsb-release ubuntu-keyring && \
curl https://nginx.org/keys/nginx_signing.key | gpg --dearmor | sudo tee /usr/share/keyrings/nginx-archive-keyring.gpg >/dev/null && \
gpg --dry-run --quiet --no-keyring --import --import-options import-show /usr/share/keyrings/nginx-archive-keyring.gpg && \
. /etc/os-release && \
echo "deb [signed-by=/usr/share/keyrings/nginx-archive-keyring.gpg] http://nginx.org/packages/mainline/ubuntu $UBUNTU_CODENAME nginx" | sudo tee /etc/apt/sources.list.d/nginx.list && \
echo -e "Package: *\nPin: origin nginx.org\nPin: release o=nginx\nPin-Priority: 900\n" | sudo tee /etc/apt/preferences.d/99nginx && \
sudo apt update && \
sudo apt install -y nginx

# .htpasswd (use plain password)
username:{PLAIN}password

# sudo vim /etc/nginx/nginx.conf
# sudo vim /etc/nginx/.htpasswd
# sudo chmod o+x $HOME

user  nginx;
worker_processes  auto;

error_log  /var/log/nginx/error.log notice;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
}

# load_module modules/ngx_http_brotli_filter_module.so;
# load_module modules/ngx_http_brotli_static_module.so;

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    # brotli on;
    # brotli_comp_level 4;
    # brotli_static on;
    # brotli_types application/atom+xml application/javascript application/json application/vnd.api+json application/rss+xml
    #             application/vnd.ms-fontobject application/x-font-opentype application/x-font-truetype
    #             application/x-font-ttf application/x-javascript application/xhtml+xml application/xml
    #             font/eot font/opentype font/otf font/truetype image/svg+xml image/vnd.microsoft.icon
    #             image/x-icon image/x-win-bitmap text/css text/javascript text/plain text/xml;

    server {
        listen       80;
        server_name  localhost;
        root   /usr/share/nginx/html;
        index  index.html;

        location /a {
            auth_basic "Restricted";
            auth_basic_user_file /etc/nginx/.htpasswd;
        }

        # error_page  404              /404.html;

        # redirect server error pages to the static page /50x.html
        #
        # error_page   500 502 503 504  /50x.html;
        # location = /50x.html {
        #     root   /usr/share/nginx/html;
        # }
    }
}

# install (ubuntu)
sudo apt update && \
sudo apt install -y curl ca-certificates libpq-dev g++ make && \
sudo install -d /usr/share/postgresql-common/pgdg && \
sudo curl -o /usr/share/postgresql-common/pgdg/apt.postgresql.org.asc --fail https://www.postgresql.org/media/keys/ACCC4CF8.asc && \
source /etc/os-release && \
sudo sh -c 'echo "deb [signed-by=/usr/share/postgresql-common/pgdg/apt.postgresql.org.asc] https://apt.postgresql.org/pub/repos/apt $(grep UBUNTU_CODENAME /etc/os-release | cut -d = -f 2)-pgdg main" > /etc/apt/sources.list.d/pgdg.list' && \
sudo apt update && \
sudo apt install -y postgresql && \
cat << EOL >> ~/.bashrc && \
export PGUSER=postgres
export LANGUAGE=en_US
export LC_ALL=en_US.UTF-8
EOL
. ~/.bashrc && \
sudo sed -i "s/peer/trust/g" /etc/postgresql/*/main/pg_hba.conf && \
sudo sed -i "s/scram-sha-256/trust/g" /etc/postgresql/*/main/pg_hba.conf && \
sudo systemctl restart postgresql


# install (mac)
brew install postgresql@17
vim ~/.zshrc
  export PATH="/opt/homebrew/opt/postgresql@17/bin:$PATH"
  export LDFLAGS="-L/opt/homebrew/opt/postgresql@17/lib"
  export CPPFLAGS="-I/opt/homebrew/opt/postgresql@17/include"
  export LC_ALL="C"
  export PGDATA=[save_dir]
initdb --locale=C -E UTF-8
pg_ctl start/stop/restart/reload/status


# データベースの作成
createdb mydb

# データベースの削除
dropdb mydb

# データベースへアクセス
psql mydb

# [psql] table list
\d

# [psql] table definition
\d table 

# [psql] exit
\q

# [psql] defrag
VACUUM or VACUUM FULL

# buckup
pg_dump dbname > dumpfile

# restore
psql dbname < dumpfile

# performance setting
cat << EOL >> /etc/postgresql/*/main/postgresql.conf
fsync = off
synchronous_commit = off
full_page_writes = off
max_wal_size = 10GB
checkpoint_timeout = 30min
EOL


# node (code)
import pg from 'pg';

const { native } = pg;
const { Client } = native;

const client = new Client({
    user: 'postgres',
    database: 'mydb',
});
await client.connect();

const res = await client.query('SELECT $1::text as message', ['Hello world!']);
console.log(res.rows[0].message); // Hello world!
await client.end();


# python (install)
pip install "psycopg[binary]"

# python (code)
import psycopg
conn = psycopg.connect("dbname=mydb user=postgres")
cur = conn.cursor()
try:
    cur.execute("create table test (id integer primary key)")
Exception as e:
    print(e)
    conn.rollback()
else:
    conn.commit()
cur.close()
conn.close()


# add column to table
BEGIN;

-- Create a new table with the desired column order
CREATE TABLE new_table (
    column1 data_type,
    new_column data_type,
    column2 data_type
);

-- Copy data from the old table to the new table
INSERT INTO new_table (column1, column2)
SELECT column1, column2
FROM old_table;

-- Drop the old table
DROP TABLE old_table;

-- Rename the new table to the old table's name
ALTER TABLE new_table RENAME TO old_table;

COMMIT;


# [old] install (mac)
vim ~/.zprofile
  export LC_ALL=C
  export PGDATA=[savedir]
  export LDFLAGS="-L/opt/homebrew/opt/postgresql@16/lib"
  export CPPFLAGS="-I/opt/homebrew/opt/postgresql@16/include"
brew install postgresql@16
vim /opt/homebrew/var/postgresql@16/postgresql.conf  // [psql] SHOW config_file;
  #data_directory = 'ConfigDir' -> data_directory = [savedir]
mkdir [savedir]
initdb --locale=C -E UTF-8 [savedir]
brew services restart postgresql@16
[psql] SHOW data_directory;
